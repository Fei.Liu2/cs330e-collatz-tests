#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "148 283\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  148)
        self.assertEqual(j, 283)

    def test_read_3(self):
        s = "729 402\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  729)
        self.assertEqual(j, 402)     

    def test_read_4(self):
        s = "837292 111111\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  837292)
        self.assertEqual(j, 111111)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    def test_eval_6(self):
        v = collatz_eval(29383, 18296)
        self.assertEqual(v, 308)

    def test_eval_7(self):
        v = collatz_eval(1, 99999)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 384, 492, 142)
        self.assertEqual(w.getvalue(), "384 492 142\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 83923, 92839, 333)
        self.assertEqual(w.getvalue(), "83923 92839 333\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 739, 283, 171)
        self.assertEqual(w.getvalue(), "739 283 171\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("392 3993\n284 284\n920 283\n4585 9283\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "392 3993 238\n284 284 105\n920 283 179\n4585 9283 262\n")

    def test_solve_3(self):
        r = StringIO("1 5\n9 10\n869 872\n386 385\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 5 8\n9 10 20\n869 872 179\n386 385 121\n")

    def test_solve_4(self):
        r = StringIO("9999 9997\n1 1999\n7 10\n2 1596\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9999 9997 180\n1 1999 182\n7 10 20\n2 1596 182\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
